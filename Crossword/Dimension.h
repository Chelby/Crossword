namespace Crossword {
	ref class Dimension {
	public :
		const int m_top = 40;
		const int m_bottom = 40;
		const int m_left = 30;
		const int m_right = 30;
		const int block_size = 20;
		const int block_font_size = 11;
		const int num_label_size = 20;
		const int question_height = 200;
		const int question_width = 250;
		const int question_top_margin = 50;
		const int question_left_margin = 30;
		const int numberOfQuestion_top_margin = 30;
		const int numberOfQuestion_height = 20;
		const int numberOfQuestion_width = 200;
		const int spaceForButton = 70;
		const int question_font_size = 10;
		const int navButton_width = 50;
		const int navButton_height = 20;
		const int distance_between_button = 200;
		const int numBox_left_margin = 15;
		const int numBox_top_margin = 45;
		const int confirmButton_margin_top = 60;
		const int confirmButton_width = 60;
		const int confirmButton_height = 30;
	};
}