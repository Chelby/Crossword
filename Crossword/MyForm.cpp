#include "MyForm.h"

namespace Crossword {
	using namespace System;
	using namespace System::Windows::Forms;

	[STAThread]
	void main(array<String^>^ args)
	{
		Application::EnableVisualStyles();
		Application::SetCompatibleTextRenderingDefault(false);
		MyForm myForm;
		Application::Run(%myForm); //������ ������� �����
	}

	//�����������
	MyForm::MyForm(void)
	{
		InitializeComponent();
		currQuestion = 0;
		d = gcnew Dimension;
	}

	//����� ������� ���������� �� ����� NewGame ����� ����� ���� ���������� ����
	//���������:
	//			type - ���������� ����� ������ � ��������� � ��������
	//			count - ���������� ��������
	//			max - ���������� �������� / ������� ������������ � �����
	//����� ��������� ����� � ��������� � �������� � ��������� �� ��� "count" ��������� ��������, � ����� �������� ����� drawCross()
	void MyForm::start(int type, int count, int max) {
		questCount = count;
		try {
			question = File::ReadAllLines(gcnew String("Q" + type + ".txt"), System::Text::Encoding::GetEncoding(1251));
			answer = File::ReadAllLines(gcnew String("A" + type + ".txt"), System::Text::Encoding::GetEncoding(1251));
			allowQuest = gcnew List<int>;

			Random^ rand = gcnew Random;
			int number;
			for (int i = 0; i < count; i++) {
				number = rand->Next(max); //������������ ��������� ����� � ��������� �� 0 �� "max"
				if (!allowQuest->Contains(number)) { //�������� �� �� �������� �� ������ "allowQuest" ������� "number"
					allowQuest->Add(number);
				}
				else {
					i--;
				}
			}

			drawCross();
		}
		catch (FileNotFoundException^ e) { //�������� ����������
			MessageBox::Show(this, "��� ������ � ���������!");
		}
	}


	//����� ��������� ������ ����� � �����������
	void MyForm::drawCross() {
		if (answer != nullptr) {
			this->Controls->Clear(); //������� ����� �� ���� ���������
			this->Controls->Add(this->menuStrip1); 
			int max_height = (questCount * d->block_size) + d->m_top + d->m_bottom > d->question_height + d->m_top + d->m_bottom ?   //����������� ������������ ������ ���������� � ������� ���������� ���������
				questCount * d->block_size + d->m_top :
				d->question_height + d->m_top + d->m_bottom;
			int max_width = answer[0]->Length;
			for (int i = 0; i < answer->Length; i++) { //����������� ������������� ���������� ������ � ����� ������ ����������
				if (max_width < answer[i]->Length)
					max_width = answer[i]->Length;
			}
			this->ClientSize = Drawing::Size(max_width * d->block_size + d->question_width + 2*d->question_left_margin + d->m_right + d->m_left, max_height + d->spaceForButton); //������� �������� ���� ������� �����

			//������������� � ��������� "numOfQuest"
			numOfQuest = gcnew Label;
			numOfQuest->Location = System::Drawing::Point(max_width * d->block_size + d->m_left + d->question_left_margin, d->numberOfQuestion_top_margin);
			numOfQuest->Size = System::Drawing::Size(d->numberOfQuestion_width, d->numberOfQuestion_height);
			numOfQuest->Text = L"������ �1";
			this->Controls->Add(numOfQuest);

			//������������� � ��������� "questionBox"
			questionBox = gcnew Label();
			questionBox->AutoSize = false;
			questionBox->Location = System::Drawing::Point(max_width * d->block_size + d->m_left + d->question_left_margin, d->question_top_margin);
			questionBox->Name = L"QuestBox";
			questionBox->Size = System::Drawing::Size(d->question_width, d->question_height);
			questionBox->Visible = true;
			questionBox->BackColor = Drawing::Color::FromArgb(255, 255, 255);
			questionBox->Text = question[allowQuest[0]];
			questionBox->Font = gcnew System::Drawing::Font(questionBox->Font->Name, d->question_font_size);
			this->Controls->Add(questionBox);

			//������������� � ��������� ������ "prev" � "next"
			next = gcnew Button;
			prev = gcnew Button;

			prev->Location = System::Drawing::Point(max_width * d->block_size + d->m_left + d->question_left_margin, d->question_height + d->question_top_margin);
			prev->Size = System::Drawing::Size(d->navButton_width, d->navButton_height);
			prev->Text = L"����.";
			prev->Enabled = false;
			prev->Click += gcnew System::EventHandler(this, &Crossword::MyForm::buttonPrev_Click);

			next->Location = System::Drawing::Point(max_width * d->block_size + d->m_left + d->question_left_margin + d->distance_between_button, d->question_height + d->question_top_margin);
			next->Size = System::Drawing::Size(d->navButton_width, d->navButton_height);
			next->Text = L"����.";
			if (question->Length <= 1)
				next->Enabled = false;
			next->Click += gcnew System::EventHandler(this, &Crossword::MyForm::buttonNext_Click);

			this->Controls->Add(prev);
			this->Controls->Add(next);

			//������������� � ��������� �������� numbers � box
			numbers = gcnew array<Label^>(questCount);
			box = gcnew array<array<TextBox^>^>(questCount);
			for(int i = 0; i < questCount; i++) {
				numbers[i] = gcnew Label();
				numbers[i]->Location = System::Drawing::Point(d->numBox_left_margin, d->numBox_top_margin + i * d->block_size);
				numbers[i]->Size = System::Drawing::Size(d->num_label_size, d->num_label_size);
				numbers[i]->Text = Convert::ToString(i + 1) + ".";
				this->Controls->Add(numbers[i]);


				box[i] = (gcnew array<TextBox^>(answer[allowQuest[i]]->Length));
				for (int j = 0; j < answer[allowQuest[i]]->Length; j++) {
					box[i][j] = gcnew TextBox();
					box[i][j]->Location = System::Drawing::Point((d->m_left/2) + d->block_size * (j + 1), d->m_top + i * d->block_size);
					box[i][j]->Size = System::Drawing::Size(d->block_size, d->block_size);
					box[i][j]->MaxLength = 1;
					box[i][j]->Font = gcnew System::Drawing::Font(box[i][j]->Font->Name, d->block_font_size);
					this->Controls->Add(box[i][j]);
					box[i][j]->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Crossword::MyForm::textBox_KeyUp);
				}
			}

			//������������� � ��������� ������ "confirm"
			confirm = gcnew Button();
			confirm->Location = System::Drawing::Point(d->m_left, box->Length * d->block_size + d->confirmButton_margin_top);
			confirm->Size = System::Drawing::Size(d->confirmButton_width, d->confirmButton_height);
			confirm->Text = L"������";
			confirm->Click += gcnew System::EventHandler(this, &Crossword::MyForm::buttonConfirm_Click);
			this->Controls->Add(confirm);
		}
	}

	//���������� ����� ������ � ������ ����������
	//�� ������������� ����������� ����� � ����� ������ �� ������
	void MyForm::textBox_KeyUp(Object^ sender, System::Windows::Forms::KeyEventArgs^ e) {
		int cur_i, cur_j;
		for (int i = 0; i < box->Length; i++) {
			for (int j = 0; j < box[i]->Length; j++) {
				if (box[i][j]->Focused)
				{
					cur_i = i;
					cur_j = j;
					break;
				}
			}
		}
		if ((cur_j + 1) < box[cur_i]->Length && box[cur_i][cur_j + 1] != nullptr && (int)e->KeyCode >= 65 && (int)e->KeyCode <= 225) {
			box[cur_i][cur_j + 1]->Focus();
		}
	}

	//���������� ������� �� ������ "����."
	//�� ����������� ������ � questionBox �� ��������� � ����������� ����� ������� �� ���������� �������
	void MyForm::buttonNext_Click(Object^ sender, EventArgs^ e) {
		currQuestion++;
		questionBox->Text = question[allowQuest[currQuestion]];
		if (currQuestion + 1 >= questCount)
			next->Enabled = false;
		if (currQuestion > 0)
			prev->Enabled = true;
		numOfQuest->Text = L"������ �" + (currQuestion + 1);
	}

	//���������� ������� �� ������ "����."
	//�� ����������� ������ � questionBox �� ��������������� � ����������� ����� ������� �� ������� �������
	void MyForm::buttonPrev_Click(Object^ sender, EventArgs^ e) {
		currQuestion--;
		questionBox->Text = question[allowQuest[currQuestion]];
		if (currQuestion == 0)
			prev->Enabled = false;
		if (currQuestion < questCount)
			next->Enabled = true;
		numOfQuest->Text = L"������ �" + (currQuestion + 1);
	}

	//���������� ������� �� ������ "������"
	//�� ��������� ��������� ������ � ��������� � ���������� �� � �� �� ��������� ����� Result ��� ��������
	void MyForm::buttonConfirm_Click(Object^ sender, EventArgs^ e) {
		array<String^>^ tempArr = gcnew array<String^>(box->Length);
		for (int i = 0; i < box->Length; i++) {
			tempArr[i] = gcnew String("");
			for (int j = 0; j < box[i]->Length; j++) { //���������� ��������� ������ �� ������ ������� ����������
				tempArr[i] += box[i][j]->Text;
			}
		}
		Result^ myResult = gcnew Result(tempArr, answer, allowQuest, this);
		myResult->Show();
	}

	//����� ������� ���������� �� ����� Result ��� ������������ ������ ���������� ������ ������ � ����������� �� ������������ ������
	//���������:
	//			arr - ������ � ���������������� ����� ��� ������ ������ ���������� (0 - �������, 1 - ������ �������)			
	void MyForm::colorize(array<int>^ arr) {
		for (int i = 0; i < arr->Length; i++) {
			if (arr[i] == 0) {
				for (int j = 0; j < box[i]->Length; j++) {
					box[i][j]->BackColor = System::Drawing::Color::FromArgb(255, 182, 193);
				}
			}
			else if (arr[i] == 1) {
				for (int j = 0; j < box[i]->Length; j++) {
					box[i][j]->BackColor = System::Drawing::Color::FromArgb(202, 255, 112);
				}
			}
		}
	}

	//���������� ������� �� ����� ���� "�������� �������"
	//� ��� �� ������� ����������� ��� OpenFileDialog ��� ������ ����� � ��������� � ����� ����� � ��������
	//����� ����� ����������� ����� InputName ��� ����� �������� �������� ��������
	System::Void MyForm::SelectQuestionFile_Click(System::Object^  sender, System::EventArgs^  e) {
		OpenFileDialog^ fileDialogQuest = gcnew OpenFileDialog();
		fileDialogQuest->Title = L"�������� ���� � ���������";
		fileDialogQuest->Filter = "Text Files|*txt";
		if (fileDialogQuest->ShowDialog() == Windows::Forms::DialogResult::OK) {
			questionContent = File::ReadAllLines(fileDialogQuest->FileName, System::Text::Encoding::GetEncoding(1251));
			OpenFileDialog^ fileDialogAnsw = gcnew OpenFileDialog();
			fileDialogAnsw->Title = L"�������� ���� � ��������";
			fileDialogAnsw->Filter = "Text Files|*txt";
			if (fileDialogAnsw->ShowDialog() == Windows::Forms::DialogResult::OK) {
				answerContent = File::ReadAllLines(fileDialogAnsw->FileName, System::Text::Encoding::GetEncoding(1251));
				InputName^ inputForm = gcnew InputName(gcnew MyDelegate(processData));
				inputForm->ShowDialog();
			}
		}
	}

	// ���������� ������� �� ����� ���� "����� ����"
	//�� ������� � ��������� ����� "NewGame" ��� ������ ���������� � ������� ����� ����
	System::Void MyForm::���������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		NewGame^ form = gcnew NewGame(this);
		form->Show();
	}

	//����� �� ������� ��������� MyDelegate
	//���������:
	//			param - ��� ����� �������� ��������
	//����� ������ � ������ ��� � ���� ����� ���� � ������, � ������� ��� ����� � ��������� � �������� � �������� ����
	void MyForm::processData(String^ param) {
		FileStream^ fs = gcnew FileStream("question_list.txt", FileMode::Append);
		StreamWriter^ writer = gcnew StreamWriter(fs, System::Text::Encoding::GetEncoding(1251));
		writer->Write(param + "\n");
		writer->Close();
		StreamReader^ reader = gcnew StreamReader("question_list.txt", System::Text::Encoding::GetEncoding(1251));
		int count = 0;
		while (reader->ReadLine() != nullptr)
			count++;
		reader->Close();
		FileStream^ newQuestion = gcnew FileStream(gcnew String("Q" + (count - 1) + ".txt"), FileMode::CreateNew); //�������� ������ ����� � ���������
		FileStream^ newAnswer = gcnew FileStream(gcnew String("A" + (count - 1) + ".txt"), FileMode::CreateNew); //�������� ������ ����� � ��������
		StreamWriter^ writerQuestion = gcnew StreamWriter(newQuestion, System::Text::Encoding::GetEncoding(1251));
		StreamWriter^ writerAnswer = gcnew StreamWriter(newAnswer, System::Text::Encoding::GetEncoding(1251));
		for each (String^ str in questionContent) { //����������� ������ �� ������ ����� � ������
			writerQuestion->WriteLine(str);
		}
		for each (String^ str in answerContent) { //����������� ������ �� ������ ����� � ������
			writerAnswer->WriteLine(str);
		}
		writerAnswer->Close();
		writerQuestion->Close();
		newQuestion->Close();
		newAnswer->Close();
	}

	//���������� ������� �� ����� ���� "������� �����������"
	//�� ������� � ��������� ����� "Statistics" � ������� �������� ���������� ��� �������
	System::Void MyForm::������������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		Statistics^ form = gcnew Statistics();
		form->Show();
	}

	//���������� ������� �� ����� ���� "�����"
	//����� ��������� ���������
	System::Void MyForm::exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}

}