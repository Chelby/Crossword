namespace Crossword {
	public interface class IColorAnswer {
		virtual void colorize(array<int>^ arr);
	};
}