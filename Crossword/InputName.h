#pragma once
#include "MyDelegate.h"

namespace Crossword {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� InputName
	/// </summary>
	public ref class InputName : public System::Windows::Forms::Form
	{
	private:
		String^ name; //��� ������
		MyDelegate^ myDel; //�������
	public:
		InputName(MyDelegate^ myDel);
		String^ getName(); //����� ������� ���������� ��� ������
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e); //���������� ������� �� ������ "��"
	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~InputName()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button1;
	protected:
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Label^  label1;

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(252, 57);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 0;
			this->button1->Text = L"��";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &InputName::button1_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(31, 59);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(201, 20);
			this->textBox1->TabIndex = 1;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label1->Location = System::Drawing::Point(28, 26);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(246, 18);
			this->label1->TabIndex = 2;
			this->label1->Text = L"������� �������� ���� ��������";
			// 
			// InputName
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(364, 137);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->button1);
			this->Name = L"InputName";
			this->Text = L"����� ����";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	
	};
}
