#pragma once
#include "IEvent.h"
namespace Crossword {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System;
	using namespace System::IO;
	using namespace System::Collections;
	/// <summary>
	/// ������ ��� NewGame
	/// </summary>
	public ref class NewGame : public System::Windows::Forms::Form
	{
	private: ArrayList^ namesOfQuestion; //������ � ������� ���������� �������� ��� ��������
			 System::Windows::Forms::Label^  label2;
			 System::Windows::Forms::TextBox^  textBox1;
			 IEvent^ action; ////������ ������� �������������� ��������� IEvent
			 int max; //������������ ���������� �������� ��������� ����

	public:
			 NewGame(IEvent^ action); //�����������
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e); //���������� ������� �� ������ "������ ����� ����"
	private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e); //���������� ������ �������� �� "comboBox1"
	private: void textBox1_KeyPress(Object^ sender, KeyPressEventArgs^ e); //���������� ����� ������ � textBox1
	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~NewGame()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::ComboBox^  comboBox1;
	private: System::Windows::Forms::Button^  button1;
	protected:

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.45F));
			this->label1->Location = System::Drawing::Point(45, 28);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(155, 15);
			this->label1->TabIndex = 0;
			this->label1->Text = L"�������� ���� ��������";
			// 
			// comboBox1
			// 
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Location = System::Drawing::Point(42, 78);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(158, 21);
			this->comboBox1->TabIndex = 1;
			this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &NewGame::comboBox1_SelectedIndexChanged);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(42, 219);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(158, 23);
			this->button1->TabIndex = 2;
			this->button1->Text = L"������ ����� ����";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &NewGame::button1_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.45F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label2->Location = System::Drawing::Point(9, 123);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(185, 15);
			this->label2->TabIndex = 3;
			this->label2->Text = L"������� ���������� ��������";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(42, 169);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(158, 20);
			this->textBox1->TabIndex = 4;
			this->textBox1->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &NewGame::textBox1_KeyPress);
			// 
			// NewGame
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(250, 274);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->comboBox1);
			this->Controls->Add(this->label1);
			this->Name = L"NewGame";
			this->Text = L"����� ����";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	
};
}
