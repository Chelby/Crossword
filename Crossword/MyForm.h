#pragma once
#include "NewGame.h"
#include "Result.h"
#include "InputName.h"
#include "Statistics.h"
#include "Dimension.h"
#include <stdio.h>
#include <stdlib.h>
#include <vcclr.h>

namespace Crossword {
	
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Collections::Generic;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Windows::Forms;
	using namespace System::Runtime::InteropServices;
	/// <summary>
	/// ������ ��� MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form, public IEvent, public IColorAnswer
	{
	private:
			 array<String^>^ question; //������ � ������� ��������� ��� ������� ��������� ����
			 array<String^>^ answer; //������ � ������� ��������� ��� ������ ��������� ����
			 Label^ questionBox; //������������ � ������� ������������ ������
			 array<array<TextBox^>^>^ box; //������ ����������
			 Button^ next; //������ "����."
			 Button^ prev; //������ "����."
			 Label^ numOfQuest; //������� �������� �������
			 array<Label^>^ numbers; //������ "Label" ��� ��������� ����� � ����������
			 Button^ confirm; //������ "������"
			 static array<String^>^ answerContent; //������ � ������� �������� ���������� ����������� ������� �� ������ ����� � ������ ��� ���������� ����� ����
			 static array<String^>^ questionContent; //������ � ������� �������� ���������� ����������� �������� �� ������ ����� � ������ ��� ���������� ����� ����
			 int currQuestion; //������� �������� ������� � questionBox
			 Dimension^ d; //������ �� ����� ������� ���������� ��� ������������� ���������� ����������
			 int questCount; //���������� �������� ��������� �������������
			 List<int>^ allowQuest; //������ � ����������� �������� ��������� ��������
	public:
			MyForm(void);
			virtual void start(int, int, int); //�������� ����������������� ������ � ���������� IEvent, ������� ������������ ��� ������ ����� ����
			virtual void colorize(array<int>^ arr); //�������� ����������������� ������ � ���������� IColorAnswer, ������� ����������� ������ ���������� ������ ������ � ����������� �� ������������ ������
			static void processData(String^ param); //����� ������� ���������� ��������
	private: void drawCross();
			 void textBox_KeyUp(Object^ sender, System::Windows::Forms::KeyEventArgs^ e); //���������� ����� ������ � ������ ����������
			 void buttonNext_Click(Object^ sender, EventArgs^ e);  //���������� ������� �� ������ "����."
			 void buttonPrev_Click(Object^ sender, EventArgs^ e);  //���������� ������� �� ������ "����."
			 void buttonConfirm_Click(Object^ sender, EventArgs^ e);  //���������� ������� �� ������ "������"
			 System::Void SelectQuestionFile_Click(System::Object^  sender, System::EventArgs^  e);  //���������� ������� �� ����� ���� "�������� �������" 
			 System::Void ���������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);  //���������� ������� �� ����� ���� "����� ����"
			 System::Void ������������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);  //���������� ������� �� ����� ���� "������� �����������"
			 System::Void exitToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e);  //���������� ������� �� ����� ���� "�����"
	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
				delete question;
				delete answer;
				delete questionBox;
				delete box;
				delete next;
				delete prev;
				delete numOfQuest;
				delete numbers;
				delete confirm;
				delete answerContent;
				delete questionContent;
				delete d;
				delete allowQuest;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  ����ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  ���������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  ����������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  �ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  ���������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  ��������������������ToolStripMenuItem;



	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->����ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->���������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->���������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->��������������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->����ToolStripMenuItem,
					this->���������ToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(223, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// ����ToolStripMenuItem
			// 
			this->����ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->���������ToolStripMenuItem,
					this->����������ToolStripMenuItem, this->�ToolStripMenuItem
			});
			this->����ToolStripMenuItem->Name = L"����ToolStripMenuItem";
			this->����ToolStripMenuItem->Size = System::Drawing::Size(46, 20);
			this->����ToolStripMenuItem->Text = L"����";
			// 
			// ���������ToolStripMenuItem
			// 
			this->���������ToolStripMenuItem->Name = L"���������ToolStripMenuItem";
			this->���������ToolStripMenuItem->Size = System::Drawing::Size(190, 22);
			this->���������ToolStripMenuItem->Text = L"����� ����";
			this->���������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::���������ToolStripMenuItem_Click);
			// 
			// ����������ToolStripMenuItem
			// 
			this->����������ToolStripMenuItem->Name = L"����������ToolStripMenuItem";
			this->����������ToolStripMenuItem->Size = System::Drawing::Size(190, 22);
			this->����������ToolStripMenuItem->Text = L"������� �����������";
			this->����������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::������������������ToolStripMenuItem_Click);
			// 
			// �ToolStripMenuItem
			// 
			this->�ToolStripMenuItem->Name = L"�ToolStripMenuItem";
			this->�ToolStripMenuItem->Size = System::Drawing::Size(190, 22);
			this->�ToolStripMenuItem->Text = L"�����";
			this->�ToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::exitToolStripMenuItem_Click);
			// 
			// ���������ToolStripMenuItem
			// 
			this->���������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->��������������������ToolStripMenuItem });
			this->���������ToolStripMenuItem->Name = L"���������ToolStripMenuItem";
			this->���������ToolStripMenuItem->Size = System::Drawing::Size(83, 20);
			this->���������ToolStripMenuItem->Text = L"���������";
			// 
			// ��������������������ToolStripMenuItem
			// 
			this->��������������������ToolStripMenuItem->Name = L"��������������������ToolStripMenuItem";
			this->��������������������ToolStripMenuItem->Size = System::Drawing::Size(178, 22);
			this->��������������������ToolStripMenuItem->Text = L"�������� �������";
			this->��������������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::SelectQuestionFile_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(223, 303);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"MyForm";
			this->Text = L"���������";
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
};
}