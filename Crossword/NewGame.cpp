#include "NewGame.h"

namespace Crossword {

	//�����������
	//� ��� ���������������� ��� ���� � ����������� comboBox1 ���������� ����� (� ����� ���������� ����� ������� ��������)
	NewGame::NewGame(IEvent^ action)
	{
		InitializeComponent();
		this->action = action;
		namesOfQuestion = gcnew ArrayList();
		StreamReader^ fin = gcnew StreamReader("question_list.txt", System::Text::Encoding::GetEncoding(1251));
		String^ str;
		while ((str = fin->ReadLine()) != nullptr) {
			namesOfQuestion->Add(str);
		}
		fin->Close();
		this->comboBox1->DataSource = namesOfQuestion;
	}

	//���������� ������� �� ������ "������ ����� ����"
	//� ��� ����������� ������������ ��������� ������ � ����� ������ start() � ������� ����� ��� ������ ����� ����
	System::Void NewGame::button1_Click(System::Object^  sender, System::EventArgs^  e) { 
		if (Convert::ToInt32(textBox1->Text).CompareTo(max) <= 0)
			action->start(comboBox1->SelectedIndex, Convert::ToInt32(textBox1->Text), max);
		else
			MessageBox::Show(this, "���������� ��������� �������� ������� �������");
		this->Close();
	}

	//���������� ����� ������ � textBox1
	//�� ��������� ������� � textBox1 ������ �����
	void NewGame::textBox1_KeyPress(Object^ sender, KeyPressEventArgs^ e)
	{
		if (!Char::IsDigit(e->KeyChar) && e->KeyChar != Convert::ToChar(8))
		{
			e->Handled = true;
		}
	}

	//���������� ������ �������� �� "comboBox1"
	//��� ������ ������ �������� � "comboBox1" �� ��������� ���������� "label2", �.�. ���������� ������������ ���������� ��������
	System::Void NewGame::comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		array<String^>^ file = File::ReadAllLines(gcnew String("Q" + comboBox1->SelectedIndex + ".txt"), System::Text::Encoding::GetEncoding(1251)); //���������� ����� � ������ ����
		max = file->Length;
		label2->Text = "������� ���������� �������� (max=" + max + ")";
	}
}