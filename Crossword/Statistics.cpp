#include "Statistics.h"
namespace Crossword {

	//�����������
	//� ��� ���������������� ������� ������ �� ����� � ������������ ��� � ��� ������ ����������
	Statistics::Statistics(void)
	{
		InitializeComponent();
		dataGridView1->AutoGenerateColumns = true;
		DataTable^ table = gcnew DataTable(); 
		//���������� ���� ����� �������� � �������
		table->Columns->Add("�");
		table->Columns->Add("���");
		table->Columns->Add("���������");

		openFile();

		//���������� ����� ������� ������� �� �����
		for (int i = 0; i < keys->Length; i++) {
			DataRow^ row = table->NewRow();
			row[0] = i + 1;
			row[1] = keys[i];
			row[2] = values[i];
			table->Rows->Add(row);
		}

		dataGridView1->DataSource = table;
		dataGridView1->ReadOnly = true;
	}

	//����� ������� ��������� � ������ ����� ������� � �� ����������
	void Statistics::openFile() {
		StreamReader^ reader = gcnew StreamReader("player_result.ini", System::Text::Encoding::GetEncoding(1251));
		String ^str1, ^str2;
		int count = getFileCount("player_result.ini") / 2;
		keys = gcnew array<String^>(count);
		values = gcnew array<Double^>(count);
		int i = 0;
		while ((str1 = reader->ReadLine()) != nullptr && ((str2 = reader->ReadLine()) != nullptr)) {
			keys[i] = str1;
			values[i] = Convert::ToDouble(str2);
			i++;
		}
		sort();
	}

	//����� ������� ���������� ���������� ����� � ����� � ��������� ������� ���������� ��� ��������
	int Statistics::getFileCount(String^ name) {
		int count = 0;
		StreamReader^ reader = gcnew StreamReader(name);
		while (reader->ReadLine() != nullptr)
			count++;
		return count;
	}

	//����� ���������� �������� keys � values � ����������� �������� �� �������� �� ������� values
	void Statistics::sort() {
		String^ tempS;
		Double^ tempD;
		//���������� ���� ��������������� �������� ������� ��������
		for (int i = 0; i < keys->Length - 1; i++) {
			for (int j = 0; j < keys->Length - 1; j++) {
				if (values[j]->CompareTo(values[j + 1]) < 0) {
					tempS = keys[j];
					tempD = values[j];
					keys[j] = keys[j + 1];
					values[j] = values[j + 1];
					keys[j + 1] = tempS;
					values[j + 1] = tempD;
				}
			}
		}
	}


}