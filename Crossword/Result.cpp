#include "Result.h"
namespace Crossword {
	//�����������
	//���������:
	//			myAnsw - ������ �� ������� ������ �������
	//			rightAnsw - ���������� ������
	//			allowQuest - ������ � ����������� �������� ��������, ������� ���� ������ ������
	//			parent - ������ ������� �������������� ��������� IColorAnswer
	//� ��� ���������������� ��� ���������� � ����������� ������ "arrColor" ������ ��� ��������� � ����������� �� ���� ��������� �� �������� �������� "myAnsw" � "rightAnsw"
	Result::Result(array<String^>^ myAnsw, array<String^>^ rightAnsw, Generic::List<int>^ allowQuest, IColorAnswer^ parent)
	{
		InitializeComponent();
		this->parent = parent;
		arrColor = gcnew array<int>(myAnsw->Length);
		max = myAnsw->Length;
		for (int i = 0; i < max; i++) {
			if ((myAnsw[i]->ToLower())->Equals(rightAnsw[allowQuest[i]]->ToLower())) { //��������� ��������� �������� � ���������� ��������
				result++;
				arrColor[i] = 1;
			}
			else {
				arrColor[i] = 0;
			}
		}
		label1->Text = "�� �������� ��������� �� " + result + "/" + max + " ��������";
	}

	//���������� ������� �� ������ "��"
	//��������� textBox1 �� ������� � ����� ����� ���������� ������ � ����������� ���� � ���� � �������� ����� "colorize()" � ������� �����
	System::Void Result::button1_Click(System::Object^  sender, System::EventArgs^  e) {
		if (textBox1->Text == String::Empty) {
			MessageBox::Show(this, "������� ���� ���");
		}
		else {
			FileStream ^fs = gcnew FileStream("player_result.ini", FileMode::Append);
			StreamWriter ^writer = gcnew StreamWriter(fs, System::Text::Encoding::GetEncoding(1251));
			writer->Write(textBox1->Text + "\n");
			writer->Write(Convert::ToString(((double)result) / max * 100) + "\n");
			writer->Close();
			parent->colorize(arrColor);
		}
		this->Close();
	}


}
